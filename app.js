
document.addEventListener('DOMContentLoaded', () => {

    //grid
    const grid = document.querySelector('.grid')
    let squares = Array.from(document.querySelectorAll('.grid div'))  
    //console.log(squares);

    //buttons
    const scoreDisplay = document.querySelector('#score')
    const startBtn = document.querySelector('#start-button')
    const width = 10 
    let miniGridRandom = 0

    //drawing tetris shapes using classList.add()
    //specify each shape rotation

    const lShape = [
        [width, width+1, width*2, width*3],
        [width*2, width*2+1, width*2+2, width*3+2],
        [width*3, width*3+1, width*2+1, width+1],
        [width*2, width*3, width*3+1, width*3+2]
    ]

    const tShape = [
        [width+1, width*2, width*2+1, width*2+2],
        [width+1, width*2, width*2+1, width*3+1],
        [width*2, width*2+1, width*2+2, width*3+1],
        [width, width*2, width*3, width*2+1]
    ]
    
    const squareShape = [
        [width, width+1, width*2, width*2+1],
        [width, width+1, width*2, width*2+1],
        [width, width+1, width*2, width*2+1],
        [width, width+1, width*2, width*2+1]
    ]

    const sShape = [
        [width, width+1, width*2+1, width*2+2],
        [width+1,width+2, width*2, width*2+1],
        [width, width*2, width*2+1, width*3+1],
        [width+1, width*2, width*2+1, width*3]
    ]

    const iShape = [
        [width, width*2, width*3],
        [width, width+1, width+2],
        [width, width*2, width*3],
        [width, width+1, width+2]
    ]

    const tetrisPieces = [lShape, tShape, squareShape, sShape, iShape]; 

    /// shapes position

    let currentPosition = 4
    let currentRotation = 0

    //select shapes randomly
    let randomGeneratedShape = Math.floor(Math.random()*tetrisPieces.length)
    //console.log(randomGeneratedShape);


    let current = tetrisPieces[randomGeneratedShape][currentRotation] ///lShape at position with fist array

    //console.log(tetrisShapes[2][2]);

    //draw the shape
    function draw() {
        current.forEach(index => {
            squares[currentPosition + index].classList.add('gameShape')
        })
    }
    
    function undraw() {
        current.forEach(index => {
            squares[currentPosition + index].classList.remove('gameShape')
        })
    }

    //make the shape move every second
    timerId = setInterval(moveDown, 1000)

    //assign functions to key codes
    function control(e){
        if(e.keyCode === 37) {
            moveLeft()
        }else if(e.keyCode === 38){
            rotate()
        }else if(e.keyCode === 39){
            moveRight()
        }else if(e.keyCode === 40){
            moveDown();
        }
    }

    document.addEventListener('keyup', control) //the event target method -> sets a function (control) happen when a specified event (keyup) happens // common targets are: element, document and window


    function moveDown() {
        undraw();
        currentPosition += width
        draw()
        freezeShapesAtBottom()
    }

    //freeze bottom
    //itemArray.some(false, true, false)
    // .some - checking that the logic is true or at least some of it - at least one has to be true
    function freezeShapesAtBottom() {
        if(current.some(index => squares[currentPosition + index + width].classList.contains('bottom'))) {
            current.forEach( index => squares [currentPosition + index].classList.add('bottom'))
            //start descending a new shape
            randomGeneratedShape = miniGridRandom
            miniGridRandom = Math.floor(Math.random()*tetrisPieces.length)
            current = tetrisPieces[randomGeneratedShape][currentRotation]
            currentPosition = 4
            draw()
            displayTheNextShape()
        }
    }

    //modulus to define the place in the grid
    //set margins of the grid to be rigid
    function moveLeft() {
        undraw();
        const isAtLeftMargin = current.some(index => (currentPosition + index) % width === 0)
        if(!isAtLeftMargin) currentPosition -= 1
        if(current.some(index => squares[currentPosition+index].classList.contains('bottom'))){
            currentPosition += 1;
        }
        draw();
    }
    
    //move shape to the right
    function moveRight() {
        undraw()
        const isAtRightMargin = current.some(index => (currentPosition+index)% width===width-1)
        if(!isAtRightMargin) currentPosition += 1
        if(current.some(index => squares[currentPosition+index].classList.contains('bottom'))){
            currentPosition -= 1;
        }
        draw();
    }
    //rotate the shape
    function rotate() {
        undraw()
        currentRotation ++
        if(currentRotation === current.length) { 
            currentRotation = 0;  //when rotation reaches the position 4 next one is back to position 0
        }
        current = tetrisPieces[randomGeneratedShape][currentRotation]
        draw();
        
    }

    //show the next shape
    const miniGridSquares = document.querySelectorAll('.mini-grid div')
    const miniGridWidth = 4
    let miniGridIndex = 1


    const nextShape = [
        [miniGridWidth, miniGridWidth+1, miniGridWidth*2, miniGridWidth*3], //l
        [miniGridWidth+1, miniGridWidth*2, miniGridWidth*2+1, miniGridWidth*2+2], //t
        [miniGridWidth, miniGridWidth+1, miniGridWidth*2, miniGridWidth*2+1], //square
        [miniGridWidth, miniGridWidth+1, miniGridWidth*2+1, miniGridWidth*2+2], //s
        [miniGridWidth, miniGridWidth*2, miniGridWidth*3] //i
    ]


    //function that displays the shape
    function displayTheNextShape() {
        //remove trace of a game shape in the entire grid
        miniGridSquares.forEach(a => {
            a.classList.remove('gameShape')
        })
        nextShape[miniGridRandom].forEach(index => {
            miniGridSquares[miniGridIndex + index].classList.add('gameShape')
        })
    }
    //add button functionality
    

});